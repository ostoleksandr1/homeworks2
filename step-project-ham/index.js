const serviceMenu = document.querySelector('.service__menu');
const servicePhotoMenu = document.querySelector('.service__menu-photo');

serviceMenu.addEventListener('click', evt => {
    const ulServiceMenuBtn = serviceMenu.querySelectorAll('.service__menu-item');

    for (elem of ulServiceMenuBtn) {
        elem.classList.toggle('active-click', false);
    }
    evt.target.classList.add('active-click');
    let active = evt.target.dataset.text;

    const divActive = servicePhotoMenu.querySelectorAll('.item-wrapper');

    for (elem of divActive) {
        if (active === elem.dataset.text) {
            elem.classList.remove('display-service-none');
            elem.classList.add('display-service')
        } else {
            elem.classList.toggle('display-service', false);
            elem.classList.toggle('display-service-none', true);
        }
    }
});


const landingArr = [
    {name: 'landing-one',
        text: 'Landing Pages'},
    {name: 'landing-two',
        text: 'Landing Pages'},
    {name: 'landing-three',
        text: 'Landing Pages'},
    {name: 'landing-four',
        text: 'Landing Pages'},
    {name: 'landing-five',
        text: 'Landing Pages'},
    {name: 'landing-six',
        text: 'Landing Pages'}
];
const wordpressArr = [
    {name: 'wordpress-one',
        text: 'Wordpress'},
    {name: 'wordpress-two',
        text: 'Wordpress'},
    {name: 'wordpress-three',
        text: 'Wordpress'},
    {name: 'wordpress-four',
        text: 'Wordpress'},
    {name: 'wordpress-five',
        text: 'Wordpress'},
    {name: 'wordpress-six',
        text: 'Wordpress'}
];
const pofigSho = [
    {name:'graphic-five',
        text:'Pofig Sho'},
    {name:'web-two',
        text:'Pofig Sho'},
    {name:'graphic-six',
        text:'Pofig Sho'},
    {name:'web-four',
        text:'Pofig Sho'},
    {name:'landing-two',
        text:'Pofig Sho'},
    {name:'wordpress-six',
        text:'Pofig Sho'},
];
const pofigShoOpyt = [
    {name: 'landing-five',
        text: 'Pofig Sho Again'},
    {name: 'wordpress-five',
        text: 'Pofig Sho Again'},
    {name: 'web-six',
        text: 'Pofig Sho Again'},
    {name: 'graphic-six',
        text: 'Pofig Sho Again'},
    {name: 'graphic-one',
        text: 'Pofig Sho Again'},
    {name: 'web-one',
        text: 'Pofig Sho Again'},

]

const amazingMenu = document.querySelector('.amazing__menu');
const arrItem = document.querySelectorAll('.amazing-pictures-item');
const btn = document.querySelector('#load1');

amazingMenu.addEventListener('click', event => {
    let ev = event.target.innerText;

    if (ev === 'All'){
        if (arrItem[2].childNodes.length < 1) {
            addItem('.landing', landingArr);
        }
        if (arrItem[3].childNodes.length < 1) {
                addItem('.wordpress', wordpressArr);
            }
       arrItem.forEach((elem) => {
        if (elem.classList.contains('display-none')){
            elem.classList.remove('display-none')
        }
       })
    } else if (ev === 'Graphic Design'){
        arrItem.forEach(elem => {
            if (!elem.classList.contains('graphic')){
                elem.classList.add('display-none');
            } else if( elem.classList.contains('display-none')) {
               elem.classList.remove('display-none');
            }
        })
    } else if (ev === 'Web Design'){
        arrItem.forEach(elem => {
            if (!elem.classList.contains('web')){
                elem.classList.add('display-none');
            } else if( elem.classList.contains('display-none')) {
                elem.classList.remove('display-none');
            }
        })
    } else if (ev === 'Landing Pages'){
        if (arrItem[2].childNodes.length < 1) {
            addItem('.landing', landingArr);
        }
        arrItem.forEach(elem => {
            if (!elem.classList.contains('landing')){
                elem.classList.add('display-none');
            } else if( elem.classList.contains('display-none')) {
                elem.classList.remove('display-none');
            }
        })
    }  else if (ev === 'Wordpress'){
        if (arrItem[3].childNodes.length < 1) {
            addItem('.wordpress', wordpressArr);
        }
        arrItem.forEach(elem => {
            if (!elem.classList.contains('wordpress')){
                elem.classList.add('display-none');
            } else if( elem.classList.contains('display-none')) {
                elem.classList.remove('display-none');
            }
        })
    }
})
let counter = 0;
btn.addEventListener('click', event => {
    btn.insertAdjacentHTML('beforebegin', '<div class="animation"></div>');

    // animation();
setTimeout(()=>{
    removeAnimation()
    if (arrItem[2].childNodes.length < 1){
        addItem('.landing', landingArr);
    }
    if (arrItem[3].childNodes.length < 1){
        addItem('.wordpress', wordpressArr);
    }

    arrItem.forEach(elem => {
        if (elem.classList.contains('display-none')){
            elem.classList.remove('display-none')
        }
    })
    counter++;
    if (counter === 2){
        addItem('.pofig', pofigSho);
        addItem('.pofigagain', pofigShoOpyt);
        btn.remove();
    }

},2000)

})

function addItem(selector, arr) {

    arr.forEach((elem) => {
        let {name,text} = elem;

        document.querySelector(selector).insertAdjacentHTML("beforeend", `<div  class="${name}">
          <div class="amazing-pictures-hover">
            <img class="circle" src="./img/Step%20Project%20Ham/logo/Ellipse%201%20copy%203.png" alt="picture">
            <img class="chain" src="./img/Step%20Project%20Ham/logo/Combined%20shape%207431.svg" alt="picture">
            <img class="circle-green" src="./img/Step%20Project%20Ham/logo/Ellipse%201.png" alt="picture">
            <div class="cub"></div>
            <h5 class="amazing-hover-title">creative design</h5>
            <h6 class="amazing-hover-text">${text}</h6>
          </div>
        </div>`)
    })
}

function animation(){
  btn.insertAdjacentHTML('beforebegin', '<div class="animation"></div>');
  setTimeout(removeAnimation, 2000);

}
function removeAnimation(){
    const an = document.querySelector('.animation');
    an.remove();
}

const myCarousel = document.querySelector('.my-carousel');
const carousel = document.querySelector('.carousel');
const btnCarouselLeft = carousel.querySelector('.left');
const btnCarouselRight = carousel.querySelector('.right');
const carouselLine = myCarousel.querySelector('.carousel-line');
const smallImages = carousel.querySelector('.small-images');
let motion = 0;

const updateImages = (activeImage, targetImage) => {
    activeImage.classList.remove('active');
    targetImage.classList.add('active');
};

btnCarouselRight.addEventListener('click', evt => {
    const activeImage = smallImages.querySelector('.small.active');
    const nextImage = activeImage.nextElementSibling;
    if (!nextImage) return;
    updateImages(activeImage, nextImage);
    motion += 1160;
    carouselLine.style.left = -motion + 'px';
});

btnCarouselLeft.addEventListener('click', evt => {
    const activeImage = smallImages.querySelector('.small.active');
    const prevImage = activeImage.previousElementSibling;
    if (!prevImage) return;
    updateImages(activeImage, prevImage);
    motion -= 1160;
    carouselLine.style.left = -motion + 'px';
});

smallImages.addEventListener('click', evt => {
    const targetImage = evt.target.closest('li');
    if (!targetImage) return;
    const activeImage = smallImages.querySelector('.small.active');
    updateImages(activeImage, targetImage);

    if (targetImage.classList.contains('alba')) {
        carouselLine.style.left = 0 + 'px';
        motion = 0;
    } else if (targetImage.classList.contains('vasy')) {
        carouselLine.style.left = -1160 + 'px';
        motion = 1160;
    } else if (targetImage.classList.contains('goga')) {
        carouselLine.style.left = -2320 + 'px';
        motion = 2320;
    } else if (targetImage.classList.contains('hasan')) {
        carouselLine.style.left = -3480 + 'px';
        motion = 3480;
    }
})

