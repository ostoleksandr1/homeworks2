'use strict'

const createNewUser = () => {
  let firstName = prompt('Enter name, please');
  let lastName = prompt('Enter lastname, please');
  let birthday = prompt('Enter your birthday,pleas (dd.mm.yyyy)');

  return {
    firstName,
    lastName,
    getLogin() {
      return (this.firstName.toLowerCase().slice(0, 1) + this.lastName.toLowerCase());
    },
    getAge() {
      let nowDay = new Date();
      let newDay = nowDay.getUTCFullYear() + ',' + birthday.slice(3, 5) + ',' + birthday.slice(0, 2);
      let birthdayInThisYear = new Date(newDay);
      if (nowDay - birthdayInThisYear >= 0) {
        return nowDay.getUTCFullYear() - birthday.slice(6);
      } else {
        return (nowDay.getUTCFullYear() - birthday.slice(6)) - 1;
      }
    },
    getPassword() {
      return this.firstName.toUpperCase().slice(0, 1) + this.lastName.toLowerCase() + birthday.slice(6);
    }
  }
}

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
