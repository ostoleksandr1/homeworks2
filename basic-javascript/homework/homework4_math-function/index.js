"use strict"

let numberOne ;
let numberTwo ;
let action ;
let result;

function getNumber () {
  numberOne = prompt('Enter first number',numberOne);
  numberTwo = prompt('Enter second number',numberTwo);

  if(isNaN(+numberOne) || isNaN(+numberTwo) || numberOne === 0 || numberTwo === 0){
    console.log('Entered invalid number. Try again.');
    getNumber();
  } else {
    return numberOne = Number(numberOne), numberTwo = Number(numberTwo);
  }
}

function getAction () {
  action = prompt('Entered action',action);
  if(action === '*' || action === '+' || action === '/' || action === '-' ){
    return action;
  } else {
    console.log('Entered invalid action. Try again.');
    getAction();
  }
}

function getResult (numberOne, numberTwo, action) {
  switch (action){
    case '*' :
      return result = numberOne * numberTwo;
      break;

    case '+' :
      return result = numberOne + numberTwo;
      break;

    case '/' :
      return result = numberOne / numberTwo;
      break;

    case '-' :
      return result = numberOne - numberTwo;
      break
  }
}

getNumber();
getAction();
console.log(getResult(numberOne, numberTwo, action));

