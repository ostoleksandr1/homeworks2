const filterBy = (arr, type) => {
    let arrTwo = [];
    if (type === 'null') {
        arrTwo = arr.filter(item => String(item) !== 'null');
    } else {
        arrTwo = arr.filter(item => typeof item !== type);
    }
    return arrTwo;
}

const arrNew = ['string', 34, '45', null, true, 0, undefined, 54, {}];
const allTypes = ['string', 'number', 'boolean', 'null', 'undefined'];
allTypes.forEach(type => console.log(filterBy(arrNew, type)));
