const btnCollection = document.querySelectorAll('button');
window.addEventListener('keydown', event => {
  for (elem of btnCollection) {
    if (elem.dataset.text === event.code) {
      elem.classList.add('btn-action');
    } else {
      elem.classList.toggle('btn-action', false);
    }
  }
});