'use strict'

const allP = document.querySelectorAll('p');
console.log(allP);

for (let key of allP){
    key.style.background = '#ff0000';
}
const optionsList = document.getElementById('optionsList');
console.log(optionsList);

const parentOptionsList = optionsList.parentElement;
console.log(parentOptionsList);

const childList = optionsList.childNodes;
console.log(childList);

const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = 'This is a paragraph';
console.log(testParagraph);

const mainHeader = document.querySelector('.main-header');
const includeMainHeaderList = mainHeader.children;
console.log(includeMainHeaderList);

for (let key of includeMainHeaderList){
    key.classList.add('nav-item');
}
console.log(includeMainHeaderList);

const sectionTitleList = document.querySelectorAll('.section-title');
console.log(sectionTitleList);

for (let key of sectionTitleList){
    key.classList.remove('section-title');
}
console.log(sectionTitleList);


