const form = document.querySelector('form');
const icon = document.querySelector('.icon');
const err = document.querySelector('p');
icon.addEventListener('click', event => {

  if (event.target.classList.contains('fa-eye-slash')) {
    event.target.classList.remove('fa-eye-slash');
    event.target.classList.add('fa-eye');
    const label = event.target.closest('label');
    const inputMy = label.querySelector('input');
    inputMy.setAttribute('type', 'password');
  } else {
    event.target.classList.remove('fa-eye');
    event.target.classList.add('fa-eye-slash');
    const label = event.target.closest('label');
    const inputMy = label.querySelector('input');
    inputMy.setAttribute('type', 'text');
  }
});

form.addEventListener('submit', event => {
  event.preventDefault();

  const inputValue = icon.querySelectorAll('input');

  if (inputValue[0].value !== '' && inputValue[1].value !== ''
    && inputValue[0].value === inputValue[1].value) {
    err.classList.remove('error');
    err.classList.add('not-error');
    alert('You are welcome');
  } else {
    err.classList.remove('not-error');
    err.classList.add('error');
  }

  inputValue[0].value = '';
  inputValue[1].value = '';
 })