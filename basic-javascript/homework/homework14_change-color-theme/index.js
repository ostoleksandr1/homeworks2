const btn = document.querySelector('button');
const link = document.querySelector('#color');

window.addEventListener('load', getTheme);
btn.addEventListener('click', changeColor);

function getTheme () {
  let theme;
  if (localStorage.getItem('theme') === null){
    localStorage.setItem('theme', link.getAttribute('href'));
  } else {
    theme = (localStorage.getItem('theme'));
    link.setAttribute('href', theme);
  }
}
function changeColor () {
   if (link.getAttribute('href') === './css/style.css'){
    link.setAttribute('href', './css/style1.css');
  } else {
    link.setAttribute('href', './css/style.css');
  }
  let theme = link.getAttribute('href');
  localStorage.setItem('theme', theme);
}