const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content');

tabs.addEventListener('click', event => {
  const ulTabs = tabs.querySelectorAll('li');

  for (elem of ulTabs) {
    elem.classList.toggle('active', false);
  }
  event.target.classList.add('active');
  let active = event.target.dataset.text;

  const ulTabsContent = tabsContent.querySelectorAll('li');

  for (elem of ulTabsContent) {
    if (active === elem.dataset.text) {
      elem.classList.remove('display-none');
      elem.classList.add('display');
    } else {
      elem.classList.toggle('display', false);
      elem.classList.toggle('display-none', true);
    }
  }
});