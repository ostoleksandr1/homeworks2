const button = document.querySelector('.button');

button.addEventListener('click',e => {
  const input = document.createElement('input');
  input.setAttribute('placeholder', 'Введіть діаметр кола');

  const buttonTwo = document.createElement('button');
  buttonTwo.innerText = 'Намалювати';

  const section = document.createElement('section');

  document.body.append(input);
  document.body.append(buttonTwo);
  document.body.append(section);

  buttonTwo.addEventListener('click', e => {
    let diameter = (input.value);
    for (let i = 0; i < 100; i++){
      let div = document.createElement('div');
      div.style.cssText = `width: ${diameter}px;
      height: ${diameter}px;
      border-radius: 50%;
      display: inline-block;`;
      div.style.background = getRandomColor();
      section.append(div);
    }

    let colDiv = section.querySelectorAll('div');
    console.log(colDiv);
    section.addEventListener('click', e => {
      console.log( (e.target));
      e.target.remove();
    })
  })
})
function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}
