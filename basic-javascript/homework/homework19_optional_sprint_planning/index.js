'use strict'

function catchToDeadline (arrOne, arrTwo, date) {

    const sumOne = arrOne.reduce((a, b) => a + b, 0);

    const sumTwo = arrTwo.reduce((a, b) => a + b, 0);

    const daysWork = Math.round(sumTwo / sumOne);

    const dayNow = new Date();

    const dayDeadline = new Date(date);

    const quantityNeedDay = Math.round((dayDeadline.getTime() - dayNow.getTime()) / 86400000 );

    const quantityNeedDayWithoutWeekEnd = Math.round(quantityNeedDay / 7 * 5);

    const dayIsWork = quantityNeedDayWithoutWeekEnd - daysWork;

    if (dayIsWork > 0) {
        console.log(`Усі завдання будуть успішно виконані за ${dayIsWork} днів до настання дедлайну!`)
    } else {
        console.log(`Команді розробників доведеться витратити додатково ${dayIsWork * (-8)} годин після дедлайну, щоб виконати всі завдання в беклозі`)
    }
}

const arr1 = [2,4,5,4,3,4,5];
const arr2 = [12, 30, 160, 47, 268, 57, 48, 89, 20, 44];

catchToDeadline(arr1, arr2, '2023-05-16');