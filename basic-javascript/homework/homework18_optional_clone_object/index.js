'use strict'

function cloneObject (myObject) {
  const clone = {};

  for (let key in myObject){
    if (typeof myObject[key] === 'object'){
      clone[key] = cloneObject(myObject[key]);
    } else {
      clone[key] = myObject[key];
    }
  }

  return clone;
}

const someObject = {
  name: 'Goy',
  lastName:'Esy',
  table: {
    math: 5,
    biology: 3,
    physics:{
      theory: 5,
      practice:{
        classes: 5,
        home:{
          left: 7,
          right: 11,
          str: 8,
        }
      }
    }
  }
}

let clObj = {};
console.log(clObj);
clObj = cloneObject(someObject);
console.log(clObj);
console.log(clObj === someObject);