import React from "react";

export default class Button extends React.Component {
  render() {
    const click = () => {
      console.log('Clicked!');
    }

    const {
      backgroundColor = {backgroundColor: 'red'}, text = 'Text', onClick = click
    } = this.props;

    return (
      <button style={backgroundColor} onClick={onClick}>{text}</button>
    )
  }
}