import React from 'react';
import PropTypes from 'prop-types';

const Modal = (props) => {
  const {
    header = 'Title',
    show = false,
    hide = () => {},
    closeButton = false,
  } = this.props;
  const showHideModal = show ? "modal modal_show" : "modal modal_hide";
  const click = (e) => {
    if (e.target.classList.contains('modal')) {
      hide();
    }
  }

  return (
    <div className={showHideModal} onClick={click}>
      <div className='modal_main'>
        {closeButton
          ? <button className='modal_close' />
          : null
        }
        <h2 className='modal_title'>{header}</h2>
        <div className='modal_text'>
          <p>{this.props.text}</p>
        </div>
        <div className='modal_actions'>
          {this.props.children}
        </div>
      </div>
    </div>
  )
}