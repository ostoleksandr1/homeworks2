import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {

    // const click = () => {
    //   console.log('Clicked!');
    // }

    const {
      backgroundColor = {backgroundColor: 'red'}, text = 'Text', onClick = click
    } = this.props;

    return (
      <button
        style={backgroundColor}
        onClick={onClick}>{text}
      </button>
    )
}

Button.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string
}