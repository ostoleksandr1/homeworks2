const url = 'https://ajax.test-danit.com/api/json/users';
const url2 = 'https://ajax.test-danit.com/api/json/posts';

async function sendRequest(url, method = "GET", options) {
    const response = await fetch(url, {
        method: method,
        ...options
    });
    return response.json();
}

async function renderCard() {
try {
    const result = await sendRequest(`${url}`);
    const response = await sendRequest(`${url2}`);

    response.map(post => {
              const newArr = result.filter((user) => {
                  if (post.userId === user.id){
                const newCard = new Card(post.title, post.body, user.name, user.email, post.id);
                newCard.creatCard()
            }
        })
    })
   }
catch (e) {
    console.log('Error: ', e)
   }
}

renderCard();

class Card{
constructor(title, text, name, email, id) {
    this.title = title;
    this.text = text;
    this.name = name;
    this.email = email;
    this.id = id;
    this.divCard = document.createElement('div');
    this.btnDelete = document.createElement('button');
    this.hName = document.createElement('h4');
    this.hTitle = document.createElement('h3');
    this.pText = document.createElement('p');
    this.btnDelete.addEventListener('click', this.deleteCard);
}

    creatCard(container = document.body) {
            this.divCard.classList.add('div__card');
            this.btnDelete.classList.add('card__delete');
            this.btnDelete.setAttribute('id', `${this.id}`);
            this.hName.innerText = this.name + "," + this.email
            this.hTitle.innerText = this.title;
            this.pText.innerText = this.text;
            this.divCard.append(this.btnDelete, this.hName, this.hTitle, this.pText);
            container.append(this.divCard);
    }
    deleteCard() {
               const card = event.target.closest(".div__card");
               fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
        method: 'DELETE',
               })
        .then(response =>{
           if (response.status === 200){
            card.remove();
        }})
   }
}

