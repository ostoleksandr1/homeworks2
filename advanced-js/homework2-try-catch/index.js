const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",

    }
];
const keysArr = ['author', 'name', 'price'];
function outputArray(arr) {
    const newArr = arr.filter(elem => {
        try {
            const currentKeys = Object.keys(elem);

            if (currentKeys.length === keysArr.length) {
                return elem;
            } else {
                let anotherArr = [];
                keysArr.forEach(key => {
                    if (!currentKeys.includes(key)){
                        anotherArr.push(key);
                    }
                })
                throw new Error(`Missing field ${anotherArr.join(',')}`);
            }
        } catch (e) {
            console.error(e)
        }
    });

    const ul = document.createElement('ul');
    newArr.forEach(elem => {
        const li = document.createElement('li');
        const low = Object.values(elem);
        li.innerText = low.join(',');
        ul.append(li);
    });
    return ul;
}

const div = document.querySelector('#root');

div.append(outputArray(books));
