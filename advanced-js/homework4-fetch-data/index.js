const url = 'https://ajax.test-danit.com/api/swapi/films ';
const url2 = 'https://ajax.test-danit.com/api/swapi/people/';
const body = document.querySelector('body');

async function sendRequest(url, method = "GET", options) {
    const response = await fetch(url, {
        method: method,
        ...options
    });
    return response.json();
}

const getFilms = async() => {
    const ul = document.createElement('ul');
    try {
        const result = await sendRequest(`${url}`);
        const response = await sendRequest(`${url2}`);

        result.map(film => {
            const newArr = response.filter((elem) => {
                if (film.characters.includes(elem.url)){
                    return true;
                }
            }).map(({name})=> (name));

            ul.insertAdjacentHTML('beforeend', `
        <li id="${film.episodeId}">
            <h1>Episode ${film.episodeId}   "${film.name}"</h1>
            <p>${film.openingCrawl}</p>
            <p>${newArr}</p>
        </li>
        `);
        })
        body.append(ul);
    }
    catch (e) {
        console.log('Error: ', e)
    }
}

getFilms();





