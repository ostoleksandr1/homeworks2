const url = 'https://api.ipify.org/?format=json';
const url2 = 'http://ip-api.com/json/';
const btn = document.querySelector('button');
btn.addEventListener('click', getIP);

async function sendRequest(url, method = "GET", options) {
      const response = await fetch(url, {
        method: method,
        ...options
    });
    return response.json();
}

async function getIP() {
    const p = document.createElement('p');
    const response = await sendRequest(url);
    const {ip} = response;
    const result = await sendRequest(`${url2}${ip}`);
    p.innerText = result.country + ',' + result.regionName + ',' + result.city + '.';
    document.body.append(p);
}

